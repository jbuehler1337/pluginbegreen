import psycopg2
import datetime

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from fetchData import fetchData

api_data = fetchData()
raum_name = api_data[0]
meeting_name = api_data[1]
# Mit Wert kann nicht gerechnet werden, da "Budget:2000 W" String
meeting_budget = api_data[2]
meeting_teilnehmer = api_data[3]
meeting_start = api_data[4]
meeting_end = api_data[5]



def insert_data_raeume():
    
    conn = None
    raeume_id = None
        
    """ insert a new vendor into the vendors table """
    sql_raeume = """INSERT INTO raeume(raeume_name, raeume_plaetze, raeume_beamer, raeume_steckdosen, raeume_lampen)
             VALUES('RAUM_U002','25','1','10','4') RETURNING raeume_id;"""
    

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(dbname='pibg', user='useriot', host='localhost', password='iotws1920')
        cur = conn.cursor()
         # execute the INSERT statement
        
        cur.execute(sql_raeume,)        
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_data_meetings():

    conn = None
    meetings_id = None

    timeS_new = meeting_start.replace('+01:00', '').replace('-', '').replace(':', '')
    time_start = datetime.datetime.strptime(timeS_new, "%Y%m%dT%H%M%S")

    timeE_new = meeting_end.replace('+01:00', '').replace('-', '').replace(':', '')
    time_end = datetime.datetime.strptime(timeE_new, "%Y%m%dT%H%M%S")

    meeting_dauer = int((time_end.hour - time_start.hour)*60)
    print (meeting_dauer)
    meeting_start_hour = int(time_start.hour)
    print (meeting_start_hour)
    meeting_start_min = int(time_start.minute)
    print (meeting_start_min)

    sql_meetings = """INSERT INTO meetings(raeume_FK,meetings_name,meetings_start_hour,meetings_start_min, meetings_dauer_min, meetings_datum, meetings_teilnehmer, meetings_budget)
            VALUES('1',%s,%s,%s,%s,%s,%s,%s) RETURNING meetings_id;"""
    
    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(dbname='pibg', user='useriot', host='localhost', password='iotws1920')
        cur = conn.cursor()
            # execute the INSERT statement
        cur.execute(sql_meetings,(meeting_name,meeting_start_hour,meeting_start_min,meeting_dauer,meeting_start,meeting_teilnehmer,meeting_budget,))
        
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

def insert_data_verbraeuche(value1,value2,value3):
    
    conn = None
    
        
    """ insert a new vendor into the vendors table """
    sql_verbrauch = """INSERT INTO meeting_verbrauch(meeting_FK,meeting_time_stamp,meeting_verbrauch_alle_beamer,meeting_verbrauch_alle_lampen, meeting_verbrauch_steckdose1, 
    meeting_verbrauch_steckdose2,meeting_verbrauch_steckdose3)
             VALUES('1',%s,'255','150',%s,%s,%s) RETURNING meeting_verbrauch_id;"""
    

    try:
        # connect to the PostgreSQL server
        conn = psycopg2.connect(dbname='pibg', user='useriot', host='localhost', password='iotws1920')
        cur = conn.cursor()
         # execute the INSERT statement
        timestamp=datetime.datetime.now()
        cur.execute(sql_verbrauch,(timestamp,value1,value2,value3,))        
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


