from dataInsert import insert_data_raeume, insert_data_meetings

# create_db( Name of the DB )
def main():
    
    # Daten aus Kalendereintrag einmalig in DB anlegen
    insert_data_raeume()
    insert_data_meetings()
    # Verbrauchsdaten aus fritz.box einmal anlegen
    # insert_data_verbraeuche()
    
    
    

if __name__ == "__main__":
    main()
