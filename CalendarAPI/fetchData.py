from __future__ import print_function
import pickle
import os.path

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/calendar']

# The ID of a sample document.
# DOCUMENT_ID = '195j9eDD3ccgjQRttHhJPymLJUCOUjs-jmwTrekvdjFE' 

def fetchData():
    """Shows basic usage of the Docs API.
    Prints the title of a sample document.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)


    service = build('calendar', 'v3', credentials=creds)


    result = service.calendarList().list().execute()
    calendar_id = result['items'][0]['id']
    result1 = service.events().list(calendarId=calendar_id).execute()
    calendar = result1['items'][2]['attendees']
    print(result1['items'][2])
   

    # Looks up how many attendees have accepted the meeting
  
    statusCount = 0
    raum_name = result1['items'][2]['location']
    meeting_name = result1['items'][2]['summary']
    meeting_budget = result1['items'][2]['description']
    meeting_start = result1['items'][2]['start']['dateTime']
    meeting_end = result1['items'][2]['end']['dateTime']
    

    for attendees in calendar:
        #print(attendees['responseStatus'])
        if attendees['responseStatus'] == 'accepted':
            statusCount += 1

   # print('Anzahl Teilnehmer:',statusCount,'Raumname:', raum_name)
   # print(meeting_start)
    return raum_name, meeting_name,meeting_budget,statusCount,meeting_start, meeting_end


    # print(result['items'][0]['attendees'][0]['responseStatus'])
