
from CalendarAPI.dataInsert import insert_data_raeume, insert_data_meetings
from Datenbank.createDB import create_db, create_user
from Datenbank.createTables import create_tables

# create_db( Name of the DB )
def create_db_structure():
    create_user('useriot')
    create_db('pibg')
    create_tables()    

# create_db( Name of the DB )
def create_db_static_values():
    
    # Daten aus Kalendereintrag einmalig in DB anlegen
    insert_data_raeume()
    insert_data_meetings()

if __name__ == "__main__":
    create_db_structure()
    create_db_static_values()
