#!/usr/bin/env python
from __future__ import division
import time
import sys
import math

#Variables (TEMP):
READ_DB_VALUE_ACT_CONSUMPTION_LAPTOPS=0 #Fiktiver Wert
READ_DB_VALUE_USED_BUDGET=200 #Fiktiver Wert

READ_DB_VALUE_USER_COUNT=22
READ_DB_VALUE_BEAMER_COUNT=1
READ_DB_VALUE_BEAMER_NEED=300 #Wattstunden
READ_DB_VALUE_LAPTOP_COUNT=16
READ_DB_VALUE_LAPTOP_NEED=35 #Wattstunden
READ_DB_VALUE_LIGHTS_COUNT=12
READ_DB_VALUE_LIGHTS_NEED=58 #Wattstunden
READ_DB_VALUE_BUDGET=3000 #Wattstunden
READ_DB_VALUE_DURATION=180 #Minuten
READ_DB_VALUE_STARTHOUR=13 #Stunden
READ_DB_VALUE_STARTMINUTE=00 #Minuten

f= open("/var/www/html/data.json","w+")
f.write('{"USER_COUNT" : "3","BEAMER_COUNT" : "1","BEAMER_NEED" : "250","LAPTOP_COUNT" : "3","LAPTOP_NEED" : "45","LIGHTS_COUNT" : "14","LIGHTS_NEED" : "54","BUDGET" : "2500","DURATION" : "180","STARTHOUR" : "17","STARTMINUTE" : "00","USED_BUDGET" : "200"}')

f.close()

class Data(object): 

    def read_static_values_from_db(self): 
        self.users = READ_DB_VALUE_USER_COUNT #TODO MIKE
        self.beamers = READ_DB_VALUE_BEAMER_COUNT #TODO MIKE
        self.beamer_need = READ_DB_VALUE_BEAMER_NEED #TODO MIKE
        self.laptops = READ_DB_VALUE_LAPTOP_COUNT #TODO MIKE
        self.laptop_need = READ_DB_VALUE_LAPTOP_NEED #TODO MIKE
        self.lights = READ_DB_VALUE_LIGHTS_COUNT #TODO MIKE
        self.lights_need = READ_DB_VALUE_LIGHTS_NEED #TODO MIKE
        self.energybudget = READ_DB_VALUE_BUDGET #TODO MIKE
        self.duration = READ_DB_VALUE_DURATION #TODO MIKE
        self.starthour = READ_DB_VALUE_STARTHOUR #TODO MIKE
        self.startminute = READ_DB_VALUE_STARTMINUTE #TODO MIKE

    def read_actual_values(self):
        self.actual = READ_DB_VALUE_ACT_CONSUMPTION_LAPTOPS #TODO MIKE
        self.used_budget = READ_DB_VALUE_USED_BUDGET #TODO MIKE


# Read the values for the logic from DB --> TODO MIKE
Data1=Data()
Data1.read_actual_values()
Data1.read_static_values_from_db()

dur_hours = int(Data1.duration / 60)
dur_minutes = Data1.duration % 60 

end_hour = Data1.starthour + dur_hours
end_minute = Data1.startminute + dur_minutes

remaining_hours=0
remaining_minutes=0

if Data1.starthour < time.localtime().tm_hour or (Data1.startminute <= time.localtime().tm_min and Data1.starthour == time.localtime().tm_hour):
    if end_hour <= time.localtime().tm_hour and end_minute>time.localtime().tm_min :
        remaining_minutes = end_minute-time.localtime().tm_min
        remaining_hours=0
    elif end_hour > time.localtime().tm_hour and end_minute <= time.localtime().tm_min:
        remaining_minutes=60-(time.localtime().tm_min - end_minute)
        remaining_hours=(end_hour-time.localtime().tm_hour)-1
    elif end_hour > time.localtime().tm_hour and end_minute > time.localtime().tm_min:
        remaining_minutes=end_minute-time.localtime().tm_min
        remaining_hours=end_hour-time.localtime().tm_hour
    else:
        print("Fehler, aktuelles Meeting laeuft nicht mehr!")
        sys.exit(0)
else:
    print("Fehler, aktuelles Meeting laeuft noch nicht!")
    sys.exit(0)


# Zusammenrechnen von Stunden und Minuten
remaining_minutes_total = remaining_hours*60 + remaining_minutes


print(remaining_minutes_total)
print(remaining_minutes_total)

# Consumption per Minute ### ECHTER WERT VS GESCHAETZTER WERT#####
if Data1.actual == 0:
    # BERECHNETE AUSWERTUNG
    consumption_per_minute=(Data1.beamers*Data1.beamer_need+Data1.laptops*Data1.laptop_need+Data1.lights*Data1.lights_need)/60

elif Data1.actual > 0: 
    # ECHTE AUSWERTUNG 
    consumption_per_minute=(Data1.actual+Data1.beamers*Data1.beamer_need+Data1.lights*Data1.lights_need) /60

# Berechnung des benoetigten Budgets fuer die restliche Meetingzeit
needed_budget= (consumption_per_minute*remaining_minutes_total)
remaining_buget=Data1.energybudget-Data1.used_budget
needed_budget_to_end=needed_budget+Data1.energybudget-remaining_buget

# Gesamtes benoetigtes Budget fuer das gesamte Meeting
needed_budget_complete_meeting=consumption_per_minute*Data1.duration

# Fehlendes Budget, positiver Wert bedeutet zu wenig Budget, negativer Wert bedeutet genug budget
missing_budget=needed_budget-remaining_buget

# Berechnung wie lange das restliche Budget noch reicht
remaining_time_budget=remaining_buget / consumption_per_minute






# Berechnung wieviel % durch Abschaltung verschiedener Teilnehmer eingespart werden kann
total_consumption_laptops=(Data1.laptops*Data1.laptop_need / 60) * Data1.duration
total_consumption_lights=(Data1.lights*Data1.lights_need / 60) * Data1.duration
total_consumption_beamers=(Data1.beamers*Data1.beamer_need / 60) * Data1.duration

switch_off_count1 = math.ceil(missing_budget/((Data1.laptop_need/60)*remaining_minutes_total))
switch_off_count2 = math.ceil(missing_budget/((Data1.lights_need/60)*remaining_minutes_total))
switch_off_count3 = math.ceil(missing_budget/((Data1.beamer_need/60)*remaining_minutes_total))


# Ausgabe der Daten --> Dies wird spaeter die Ausgabe fuer das Web-Front-End
print "#########################################################################################\n"
print "AUSWERTUNG:\n"

print ("Anfangsuhrzeit Meeting:             ", Data1.starthour, ":", Data1.startminute,
    "Uhr\nAktuelle Uhrzeit:                   ", time.localtime().tm_hour,":",time.localtime().tm_min,"Uhr",
    "\nDauer des Meetings:                 ", Data1.duration,
    "Minuten\nEingetragenes Budget:               ", int(Data1.energybudget),
    "Wh\nGesamt benoetigtes Budget:           ", int(needed_budget_to_end),
    "Wh\nBereits verbrauchtes Budget         ", int(Data1.used_budget),
    "Wh\nBenoetigtes Budget bis Ende Meeting: ", int(needed_budget),
    "Wh\nFehlendes Budget                    ", int(missing_budget),"Wh",
    "\nuebrige Meeting Zeit                 ", int(remaining_minutes_total),"Minuten",
    "\nDas Budget reicht noch fuer          ", int(remaining_time_budget),"Minuten" )
print(("Alle", Data1.laptops , "Laptops verbrauchen:        " ,Data1.laptops*Data1.laptop_need, "W/h"))
print(("Gemessener Verbrauch pro Stunde:    ", Data1.actual))
print(("Verbrauch pro Stunde:               ", consumption_per_minute*60))


print(("Verbrauch Laptops ges. Meeting:     " ,round((100/needed_budget_complete_meeting)*total_consumption_laptops),"%"))
print(("Verbrauch Lichter ges. Meeting:     " ,round((100/needed_budget_complete_meeting)*total_consumption_lights),"%"))
print(("Verbrauch Beamer ges. Meeting:      " ,round((100/needed_budget_complete_meeting)*total_consumption_beamers),"%"))

if needed_budget > remaining_buget and remaining_buget > 0: print("\nGELB --> WARNUNG!!!!!!-->BUDGET REICHT NICHT BIS ZUM ENDE!\n")
elif remaining_buget <= 0: print("RED --> FEHLER!!!!!!-->BUDGET BEREITS AUFGEBRAUCHT! MEETING ABBRECHEN!\n")
else: print("GREEN --> ES REICHT BIS ZUM ENDE!")


#print (switch_off_count1,switch_off_count2,switch_off_count3)
if switch_off_count1 > Data1.laptops and switch_off_count2 > Data1.lights and switch_off_count3 > Data1.beamers:
    print(("1. Empfehlung: Es sollten fuer die restliche Zeit von", remaining_minutes_total , "Minuten, sollten alle Laptops abgeschaltet werden und zusaetzlich Licht und Beamer sparsam verwendet werden!"))
else:
    if switch_off_count1<= Data1.laptops: print(("1. Empfehlung: Es sollten fuer die restliche Zeit von", remaining_minutes_total , "Minuten, mindestens", switch_off_count1, "Laptops abgeschaltet werden."))
    else: print(("1. Empfehlung: Es sollten fuer die restliche Zeit von", remaining_minutes_total , "Minuten, alle", Data1.laptops, "Laptops abgeschaltet werden."))
    if switch_off_count2<= Data1.lights:  print(("ODER\n2. Empfehlung: Es sollten fuer die restliche Zeit von", remaining_minutes_total , "Minuten, mindestens", switch_off_count2, "Lichter abgeschaltet werden."))
    if switch_off_count3<= Data1.beamers: print(("ODER\n3. Empfehlung: Es sollten fuer die restliche Zeit von", remaining_minutes_total , "Minuten, mindestens", switch_off_count3, "Beamer abgeschaltet werden."))

print("\n#########################################################################################")

