import psycopg2
import datetime
from Datenbank.config import config

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

def get_values():
    """ query data from the vendors table """
    conn = None
    try:
        params = config()
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.execute("SELECT meeting_verbrauch_steckdose3 FROM meeting_verbrauch")
        print("Table: ", cur.rowcount)
        row = cur.fetchone()
        row1=0
 
        while row is not None:
            print row[0],row1
        
            row = cur.fetchone()
            row1=row1+row[0]
 
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


get_values()
