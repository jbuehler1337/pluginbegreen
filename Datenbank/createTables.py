#!/usr/bin/python
 
import psycopg2
from config import config
 
 
def create_tables():
    """ create tables in the PostgreSQL database"""
    commands = (
        """
        CREATE TABLE raeume (
            raeume_id SERIAL PRIMARY KEY,
            raeume_name VARCHAR(255) NOT NULL,
            raeume_plaetze INTEGER,
            raeume_beamer INTEGER,
            raeume_steckdosen INTEGER,
            raeume_lampen INTEGER
            )
        """,
        """
         CREATE TABLE meetings (
                meetings_id SERIAL NOT NULL PRIMARY KEY,
                raeume_FK INTEGER,
                meetings_name VARCHAR(50),
                meetings_start_hour INTEGER,
                meetings_start_min INTEGER,
                meetings_dauer_min INTEGER,
                meetings_datum DATE,
                meetings_teilnehmer INTEGER,
                meetings_budget VARCHAR(50),
                FOREIGN KEY(raeume_FK)
                    REFERENCES raeume (raeume_id)
                    ON UPDATE CASCADE ON DELETE CASCADE
                )
        """
        ,
        """
        CREATE TABLE meeting_verbrauch (
                meeting_verbrauch_id SERIAL NOT NULL PRIMARY KEY,
                meeting_FK INTEGER,
                meeting_time_stamp TIMESTAMP,
                meeting_verbrauch_alle_beamer INTEGER,
                meeting_verbrauch_alle_lampen INTEGER,
                meeting_verbrauch_steckdose1 INTEGER,
                meeting_verbrauch_steckdose2 INTEGER,
                meeting_verbrauch_steckdose3 INTEGER,
                FOREIGN KEY (meeting_FK)
                    REFERENCES meetings (meetings_id)
                    ON UPDATE CASCADE ON DELETE CASCADE
        )
        """
        )
       
    conn = None
    try:
        # read the connection parameters
        params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

if __name__ == "__main__":
    create_tables()
