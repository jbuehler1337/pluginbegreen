# DB creation Skript

This project includes some Python skripts to create a PostgreSql Database for the IOT-Project

## Installation

1. Install Python and PostgreSql
2. Create a new User for PostgreSql in Postgres
     => change the configs in database.ini and createDB.py Skript
3. run commend below to create the Database named 'pibg' by default 
   (the name can also be changed in the createDB.py Skript

```bash
python main.py
```

## Tables

Table raeume:

raeume (raeume_id SERIAL PRIMARY KEY,raeume_name VARCHAR(255) NOT NULL,raeume_plaetze INTEGER,raeume_beamer INTEGER,raeume_steckdosen INTEGER,raeume_beleuchtung INTEGER)

Table meetings:
 
CREATE TABLE meetings (meetings_id SERIAL NOT NULL PRIMARY KEY,meetings_dauer TIME,meetings_datum DATE,meetings_teilnehmer INTEGER,meetings_budget VARCHAR(50),FOREIGN KEY(meetings_id)REFERENCES raeume (raeume_id)ON UPDATE CASCADE ON DELETE CASCADE)

Table meeting_verbrauch:
        
meeting_verbrauch (
                meeting_verbrauch_id SERIAL NOT NULL PRIMARY KEY,
                meeting_verbrauch_beamer VARCHAR(50),
                meeting_verbrauch_steckdose VARCHAR(50),
                meeting_verbrauch_restzeit TIME,
                FOREIGN KEY (meeting_verbrauch_id)
                    REFERENCES meetings (meetings_id)
                    ON UPDATE CASCADE ON DELETE CASCADE)

## Realtionen

Ein Meeting findet in genau einem Raum statt
In einem Raum können mehrere Meetings stattfinden
Ein Meeting hat mehrere Verbräuche und ein Verbrauch gehört zu einem Meeting