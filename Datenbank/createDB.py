
# ----- Python program to create a pibg empty database in PostgreSQL using Psycopg2 -----
#Install psycopg2 first 

# import the PostgreSQL client for Python

import psycopg2

from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

# Connect to PostgreSQL DBMS
def create_db (nameDB):
#On RaspberryPI use the user u created or the default postgres user created by postgreSql
    con = psycopg2.connect(dbname='postgres', user='useriot', host='localhost', password='iotws1920')

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    # Obtain a DB Cursor

    cursor          = con.cursor()

    name_Database   = nameDB

    # Create table statement

    sqlCreateDatabase = "CREATE DATABASE "+name_Database+";"

    
    # Create a table in PostgreSQL database

    cursor.execute(sqlCreateDatabase)


def create_user (nameUser):
    con = psycopg2.connect(dbname='postgres', user='postgres', host='localhost', password='iotws1920')

    con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

    # Obtain a DB Cursor

    cursor          = con.cursor()

    name_User   = nameUser

    # Create table statement

    sqlCreateUser = "CREATE USER "+name_User+" SUPERUSER PASSWORD 'iotws1920';"    
    # "CREATE USER userIOT SUPERUSER PASSWORD 'iotws1920';"
    
    # Create a table in PostgreSQL User incl. privileges

    cursor.execute(sqlCreateUser)
