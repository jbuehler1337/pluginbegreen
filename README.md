![PibG](https://gitlab.com/jbuehler1337/pluginbegreen/raw/master/Media/PIBG_Logo.png)



**Plug In Be Green ist ein Projekt von Studierenden der Hochschule Reutlingen für die Lehrveranstaltung Internet of Things**

Das Ziel der IoT-Vorlesung ist es, mit Technologien aus dem IOT-Umfeld arbeiten zu können, sowie die Inhalte zu analysieren und implementieren. Mit Methoden aus dem Software Engeneering wird das Projekt umgesetzt und dokumentiert. Des Weiteren soll auf diese Weise der Bezug zur Vorlesung hergestellt werden. In einem zwei tätigen Hackathon, haben Studierende Zeit, das gelernte Wissen umzusetzen und ein eigenständis Projekt auf die Beine zu stellen.

**Projekt PlugInBeGreen:** 

Die Projektidee soll den Campus mit der Auswertung von Sensoren intelligenter machen und die Räume vernetzen. Durch die Überwachung der Energieverbräuche und das gleichzeitige Sammeln der Daten von Sensorwerten in einer Datenbanken, soll das Abschalten von nicht genutzten Verbrauchern empfohlen werden. Als Verbraucher werden Geräte wie Laptops, Beamer oder Licht gezählt. Außerdem soll dadurch das Ziel erreicht werden, den Energieverbrauch des Campus zu senken sowie die Effizienz zu steigern. Die eigentliche Nutzung der Räume soll dadurch nicht negativ beeinflusst werden. Des Weiteren sollen Medienbrüche geziehlt verhindert werden. 

**Auf Youtube wurde das Werbe- und Erklärvideo unseres Unternehmens veröffentlicht. Einfach auf das Bild klicken:** 


[![](https://i.ytimg.com/vi/WqcAZ7zWMow/hqdefault.jpg)](https://youtu.be/WqcAZ7zWMow "CLICK HERE")

---


**Hackathon 28.01. / 29.01.**

Die technische Umsetzung und Abschlusspräsentation findet am 28.01. / 29.01.20 in Form eines Hackathons in Böblingen am HHZ statt. Für die Umsetzung halten wir uns an den in der Vorlesung gelehrten Inhalten. Darunter fallen folgende Punkte:

**Allgemeines:**
* Teamgründung
* Konzeption / Anwendungsidee
* Value Proposition u. Serviceentwurf
* Begründung, Beschreibung MVP
* Vorgehen und Prototyp-Implementierung
* Präsentation und Demonstration
* Repository mit Doku und Code

**Bezug zur Vorlesung:**
* Bezug zu IoT Konzepten aus VL 
* IoT Anwendung als hybride Lösung
* IoT Schichtenarchitektur u. Komm.-Verhalten
* Wie ist die Einbettung von Smart Object Computern in dem Projekt realisiert
* Wie sind IoT Design Prinzipien umgesetzt?
* Welche Medienbrüche werden durch eingebettete digitale Services gemindert?

**Videobeitrag:**
* Erklärung des Hackathon-Themas im Video

---  

**Teilnehmer der Projektgruppe:**
* Jan Bühler
* Mike Wilhelm
* Armin Kölblin

**Definition der Verantwortlichkeiten**

Noch während dem ersten zweitätigen Block der Vorlesung, wurde das Projektteam bestehend aus den oben gennanten Teilnehmern gegründet. Im direkten Anschluss an den Vorlesungsblock fand ein erstes Meeting zur Projektfindung statt. Es wurden mehrere Ideen gesammelt und festgestellt, dass sich durch die unterschiedlichen Fachkompetenzen Synergieeffekte ergeben. Am Ende des Meetings stand der Entschluss für die Umsetzung für eine Handlungsempfehlung für den Energieverbauch am Campus fest.
Des Weiteren wurde für jedes Projektmitglied Verantwortlichkeiten definiert. Die Einarbeitung in die jeweiligen Themen fand gemeinsam statt, das im Falle eines Ausfalls eines Projektmitglieds, die Verantwortung übernommen werden kann.
Folgende Verantwortlichkeiten wurden definiert:

**Verantwortliche für die verschiedenen Bereiche:**

*Jan Bühler:* Backend, Datenauswertung, Hardwareinstallation

*Armin Kölblin:* Frontend, Dokumentation

*Mike Wilhelm:* Frontend, Datenmanagement, Google API

-----

**Aufbau der Ordnerstruktur**

__CalendeAPI:__ Beinhaltet die Schnittstelle zu unserem Google Testkalender. Über diese API werden die gesamten Meetingdaten aus dem Kalender gezogen. Beispiele für diese Daten sind Anzahl an Teilnehmern, wann und wo das Meeting stattfindet usw. 

__Datenbank:__ Beinhaltet Python-Skripte zur Erstinstallation und dem Aufbau der Datenbank. 

__Media:__ Dieser Ordner beinhaltet keinen Code sondern Daten wie Logos und Präsentationen unseres Unternehmens. 

__Verbrauchsdaten:__ Hier werden mit Python-Skripten die Verbrauchswerte analysiert. Anschließend werden Berechnungen durchgeführt um dem Nutzer Empfehlungen über die weitere Vorgehensweise auszugeben. 

__html:__ Ordner für den Front- und Backendcode. Die Anzeige soll einfach per Webseite aufgerufen werden können, das Backend läuft auf dem Raspberry Pi mit einem Apache Server und nutzt die Auswertung der Verbrauchsdaten. 

-----

**Für weitere Infos zum Projekt geht's hier zur ausführlichen Projektdokumentation:
[PlugInBeGreen-Wiki](https://gitlab.com/jbuehler1337/pluginbegreen/-/wikis/home)**
