
from CalendarAPI.dataInsert import insert_data_raeume, insert_data_meetings


if __name__ == "__main__":
    # Daten aus Kalendereintrag einmalig in DB anlegen
    insert_data_raeume()
    insert_data_meetings()
